﻿/* Replaced with BranchHelperXmlImpl - reads xml file taken from 
 * http://boi.org.il/he/BankingSupervision/BanksAndBranchLocations/Pages/LocatingBankBranches.aspx
 * 
 * 
package shayja.bank.israel;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import shayja.utils.ConfigSettings;

public class BranchHelperTextFileImpl implements BranchHelper {

	private static final Map<Integer,ArrayList<Branch>> listOfBranches = new HashMap<Integer,ArrayList<Branch>>();
	private static final String BRANCHES_FILE_PATH_KEY = "branches_file_path";
	
	static {
		try {
			init();
		} catch (Exception e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
	}

	private synchronized static void init() throws Exception {
		try {
			readLargerTextFile(BRANCHES_FILE_PATH_KEY);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}




	private static void readLargerTextFile(String fileName) throws IOException {
		Path path = Paths.get(ConfigSettings.getString(fileName));
		try (BufferedReader reader = Files.newBufferedReader(path, StandardCharsets.UTF_8)){
			String line = null;
			while ((line = reader.readLine()) != null) {
				//process each line.
				String[] splitLine = line.split("\t", -1);
				int bankCode = Integer.parseInt(splitLine[0]);
				
				if (!listOfBranches.containsKey(bankCode)) {
					listOfBranches.put(bankCode, new ArrayList<Branch>());
				}
				
				Branch branch = new Branch(splitLine[2]);
				branch.setName(splitLine[3]);
				branch.setBankName(splitLine[1]);
				branch.setAddress(splitLine[4]);
				branch.setZipCode(splitLine[5]);
				branch.setCity(splitLine[6]);
				branch.setPhone(splitLine[7]);
				branch.setFax(splitLine[8]);
				branch.setClosedDate(splitLine[11]);


				listOfBranches.get(bankCode).add(branch);
				//System.out.println(line);

			}      
		}
	}

	@Override
	public ArrayList<Branch> getBranchesByBank(int bankCode) {
		return listOfBranches.get(bankCode);
	}
 
	@Override
	public Branch getBranch(int bankCode, int branchCode) {
		Branch branch = getBranchesByBank(bankCode).get(branchCode);
		return branch; 
	}

}*/
