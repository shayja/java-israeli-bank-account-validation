﻿package shayja.bank.israel;

import java.util.ArrayList;

public interface BranchHelper {

	public abstract ArrayList<Branch> getBranchesByBank(int bankCode);
	
	public abstract Branch getBranch(int bankCode, int branchCode);

}