﻿package shayja.bank.israel;

import java.io.File;
import java.util.HashMap;
import java.util.Map;
import javax.xml.parsers.*;
import org.w3c.dom.*;

import shayja.bank.israel.strategy.Strategy;
import shayja.utils.ConfigSettings;
/**
 * Contains all methods used to lookup strategies
 * 
 * @author Shay Jacoby (shayja@gmail.com)
 * 
 */
public class BankHelperImpl implements BankHelper{

	private static final String CLASS_NAME_TEMPLATE = "shayja.bank.israel.strategy.";
	private static final String BANKS_FILE_PATH_KEY = "banks_file_path";
	private static final Map<Integer,Bank> listOfBanks = new HashMap<Integer,Bank>();
	static {
		try {
			init();
		} catch (Exception e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
	}

	private static int[] splitToArray ( final String input )
	{
		if (input.isEmpty())
		{
			return null;
		}
		else
		{
			String[] strings = input.split(",");
			int[] ints = new int[strings.length];
			for (int i = 0; i < strings.length; i++) {
				ints[i] = Integer.parseInt(strings[i]);
			}
			return ints;
		}
	
	}

	private static void readBankPropertiesFromXML(final String xml) {

		try {

			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			DocumentBuilder db = dbf.newDocumentBuilder();
			File file = new File(xml);
			Document doc = db.parse(file);

			//System.out.println("Root element :" + doc.getDocumentElement().getNodeName());

			if (doc.hasChildNodes()) {
				NodeList nodeList = doc.getElementsByTagName("bank");
				Bank bank = null;
				for (int count = 0; count < nodeList.getLength(); count++) {

					Node bankNode = nodeList.item(count);

					// make sure it's element node.
					if (bankNode.getNodeType() == Node.ELEMENT_NODE) {

						if (bankNode.getNodeName().equals("bank")){
							//System.out.println("Start bank");

							if (bankNode.hasAttributes()) {

								// get attributes names and values
								NamedNodeMap nodeMap = bankNode.getAttributes();

								String id = nodeMap.getNamedItem("id").getNodeValue();
								if (id == null) continue;
								bank = new Bank(id);

								String validresultAtt = nodeMap.getNamedItem("validresult").getNodeValue();
								if (null == validresultAtt)
								{
									throw new IllegalArgumentException("aaa");
								}
								bank.setValidResult(splitToArray(validresultAtt));

								String validatorAtt = nodeMap.getNamedItem("validator").getNodeValue();
								if (null == validatorAtt)
								{
									throw new IllegalArgumentException("validator");
								}
								
								bank.setValidator(getStrategyInstance(validatorAtt));
								bank.setName(nodeMap.getNamedItem("name").getNodeValue());

								int divResult;
								Node divResultAtt = nodeMap.getNamedItem("divresult");
								if (null == divResultAtt){
									divResult = 0;
								}
								else
								{
									divResult = Integer.parseInt(divResultAtt.getNodeValue());
								}

								bank.setDivResult(divResult);

								NodeList childNodes = bankNode.getChildNodes();

								for (int i = 0; i < childNodes.getLength(); i++) {

									Node node = childNodes.item(i);

									if (node.getNodeName().equals("branches") && node.getNodeType() == Node.ELEMENT_NODE){

										NodeList branchNodes = node.getChildNodes();
										for (int j = 0; j < branchNodes.getLength(); j++) {
											String nodeName = branchNodes.item(j).getNodeName();
											
											if (nodeName.equals("active") && node.getNodeType() == Node.ELEMENT_NODE){
												String activeBranchesList = branchNodes.item(j).getTextContent();
												if (null!=activeBranchesList)
												{
													bank.setBranches(splitToArray(activeBranchesList));
												}
											}
											else if (nodeName.equals("closed") && node.getNodeType() == Node.ELEMENT_NODE){
												String closedBranchesList = branchNodes.item(j).getTextContent();
												if (null!=closedBranchesList)
												{
													bank.setClosedBranches(splitToArray(closedBranchesList));
												}
											}
										}
									}
									else if (node.getNodeName().equals("validresults")){
										if (node.hasChildNodes())
										{
											Map<int[], int[]> validBranchResults = new HashMap<int[], int[]>();
											NodeList validResultsNodes = node.getChildNodes();
											for (int k = 0; k < validResultsNodes.getLength(); k++) {
												String vrNodeName = validResultsNodes.item(k).getNodeName();
												
												if (vrNodeName.equals("result")){
													NamedNodeMap validResultsAtt = validResultsNodes.item(k).getAttributes();
													
													String listOfValidResults = validResultsAtt.getNamedItem("value").getNodeValue();
													String listOfResultBranches = validResultsNodes.item(k).getTextContent();
		
													validBranchResults.put(splitToArray(listOfValidResults), splitToArray(listOfResultBranches));
												}
												
											}
											bank.setValidBranchResults(validBranchResults);
										}
									}
								}
							}

						}
						if (bankNode.getNodeName().equals("bank")){
							listOfBanks.put(bank.getBankCode(), bank);
							//System.out.println("Close bank");
							
						}
						//System.out.println("Node Name =" + tempNode.getNodeName() + " [CLOSE]");

					}

				}


			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}



	private synchronized static void init() throws Exception {
		readBankPropertiesFromXML(ConfigSettings.getString(BANKS_FILE_PATH_KEY));

	}

	private static Strategy getStrategyInstance(final String strategyId) {
		String className = CLASS_NAME_TEMPLATE + strategyId;
		Strategy strategy = null;

		try {
			Class<?> clazz = Class.forName(className);
			strategy = (Strategy) clazz.newInstance();
		} catch (ClassNotFoundException e) {
			strategy = null;// new DefaultStrategy(IBankAccountValidator.BANK_NOT_SUPPORTED);
			System.err.println("Strategy not found [strategyId='"+ strategyId + "']");
			throw new InternalError();
		} catch (InstantiationException e) {
			System.err.println("InstantiationException while loading strategy [strategyId='"+ strategyId + "']");
			throw new InternalError();
		} catch (IllegalAccessException e) {
			System.err.println("IllegalAccessException while loading strategy [strategyId='" + strategyId + "']");
			throw new InternalError();
		}

		return strategy;
	}

	/* (non-Javadoc)
	 * @see shayja.bank.israel.BankHelper#getBankByCode(int)
	 */
	@Override
	public Bank getBankByCode(int bankCode) {

		if (!listOfBanks.containsKey(bankCode)) {
			return null;
		}

		return listOfBanks.get(bankCode);

	}




}
