﻿package shayja.bank.israel;

import shayja.utils.ArrayUtils;
import shayja.utils.StringUtils;

/**
 * Standard implementation of {@link BankAccountValidator}
 */
public class BankAccountValidatorImpl implements BankAccountValidator {

	/** Used to lookup calculation strategies for bank codes */
	private BankHelper bankHelper;
	
	public BankAccountValidatorImpl()
	{
		this.setBankHelper(new BankHelperImpl());
	}
	
	public void setBankHelper(BankHelper bankHelper) { this.bankHelper = bankHelper; }
	 
	public BankHelper getBankHelper() {
		if (null == bankHelper) {
			throw new IllegalStateException("BankHelper reference is null. There must be an implementation of BankHelper interface.");
		}
		return bankHelper;
	}


	/**
	 * @see BankAccountValidator#validateBankAccount(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public Status validateBankAccount(String bankCode, String branchCode, String accountCode) throws IllegalArgumentException {

		Bank bank;
		Branch branch;
		Account account;
		try {
			bank = new Bank(bankCode);
			branch = new Branch(branchCode);
			account = new Account(accountCode);
		} catch (Exception e) {
			// e.printStackTrace();

			if (StringUtils.isInteger(e.getMessage())) {
				Status status = Status.getStatus(Integer.parseInt(e.getMessage()));
				if (status != null) {
					return status;
				}
			}
			return Status.FAILED;
		}

		return validateBankAccount(bank, branch, account);
	}



	 
	
	
	
	/**
	 * @see BankAccountValidator#validateBankAccount(shayja.bank.israel.Bank, shayja.bank.israel.Branch, shayja.bank.israel.Account)
	 */
	@Override
	public Status validateBankAccount(Bank bank, Branch branch, Account account) {

		Status status = null;
		
		// Check non empty input bank.
		if (null == bank) {
			status = Status.EMPTY_BRANCH_CODE;
			//throw new IllegalArgumentException("Null pointer supplied as bank code.");
		}

		// Check non empty input branch
		if (null == branch) {
			status = Status.EMPTY_BRANCH_CODE;
			// throw new IllegalArgumentException("Null pointer supplied as branch code.");
		}

		// Check non empty input account
		if (null == account) {
			status = Status.EMPTY_ACCOUNT_CODE;
			// throw new IllegalArgumentException("Null pointer supplied as account code.");
		}

		if (null == status) {
			// Get the bank code by its id.
			bank = bankHelper.getBankByCode(bank.getBankCode());
		}
	
		// if bank not found in list => return BANK_NOT_SUPPORTED status.
		if (null == bank) {
			status = Status.BANK_CODE_NOT_SUPPORTED;
		}
		
		if (null == status)
		{
			// Check if branch exists on the specific bank branches list.
			if (!ArrayUtils.contains(bank.getBranches(), branch.getBranchCode())) {
				// If bank has non empty list of closed branches and this branch number found on this list => return BRANCH_CLOSED status.
				if (bank.getClosedBranches() != null && ArrayUtils.contains(bank.getClosedBranches(), branch.getBranchCode())) {
					status = Status.BRANCH_CLOSED;
				} else {
					// branch code not found on branches list nor closed branches list.
					status = Status.BRANCH_CODE_DOES_NOT_EXISTS;
				}
			}
		}
		
		if (null == status) {
			status = bank.getValidator().calculateCheckDigit(bank, branch, account);
		}

		return status;
	}

	/**
	 * Simple command line interface for israel bank check.
	 * 
	 * @param args
	 *            First argument is the bank code, second argument is the branch code, third argument is the account code
	 */
	public static void main(String[] args) {

		/*
		BranchHelper ff = new BranchHelperXmlImpl();
		Branch b = ff.getBranch(31, 45);
		System.out.println(b.getAddress());
		*/
		
		
		args = new String[] { "10", "603", "11012010083" };
		
		if (args.length < 2) {
			if (args.length < 1) {
				System.err.println("No bank code was entered. You must provide a bank code as a parameter.");
				System.out.println();
			}
			if (args.length < 2) {
				System.err.println("No account code was entered. You must provide an account code as parameter.");
				System.out.println();
			}
			System.out.println("shayja israel bank account check");
			System.out.println();
			System.out.println("Usage: java -jar bank_israel.jar <bank code> <branch code> <account code>");
			System.exit(1);
		}
		BankAccountValidatorImpl bav = new BankAccountValidatorImpl();
		Status validity = bav.validateBankAccount(args[0], args[1], args[2]);
		System.out.println(validity);

		args = new String[] { "11", "085", "426334" };
		validity = bav.validateBankAccount(args[0], args[1], args[2]);
		System.out.println(validity);

		args = new String[] { "11", "20", "426334" };
		validity = bav.validateBankAccount(args[0], args[1], args[2]);
		System.out.println(validity);

	}
}
