﻿package shayja.bank.israel;

public interface BankHelper {

	public abstract Bank getBankByCode(int bankCode);

}