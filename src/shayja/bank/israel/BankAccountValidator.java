﻿package shayja.bank.israel;


public interface BankAccountValidator {

	Status validateBankAccount(String bankCode,String branchCode, String accountCode) throws IllegalArgumentException;

    Status validateBankAccount(Bank bank, Branch branch, Account account);
}
