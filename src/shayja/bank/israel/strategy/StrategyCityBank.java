﻿package shayja.bank.israel.strategy;

import shayja.bank.israel.Account;
import shayja.bank.israel.Bank;
import shayja.bank.israel.Branch;
import shayja.bank.israel.Status;


public class StrategyCityBank implements Strategy {

    public Status calculateCheckDigit(final Bank bank,final Branch branch, final Account account) {

    	 // הערה: מס' חשבון בסיטי בנק הינו בן 8 ספרות + ספרת בקורת.
        // משתנים A B C D E F G H - X
    	long accountNr = account.getAccountCode();
        
        // חובה לוודא שמספר החשבון כולל לא פחות מ-9 ספרות
        if (accountNr / 10000000 == 0)
        {
            return Status.ACCOUNT_CODE_TOO_SHORT;
        }

        // ספרת ביקורת - משתנה X
        int checkDigit = (int)(accountNr % 10);

        // משתנים A B C D E F G H
        accountNr = accountNr / 10;

        int n = 1;
        long sum = 0;
        int dig;
        // Counting from the rightmost digit, and moving left.
        while (n < 7)
        {
            dig = (int)(accountNr % 10);
            sum = sum + (dig * (n + 1));
            accountNr = accountNr / 10;
            n++;
        }

        n = 1;
        while (n < 3)
        {
            dig = (int)(accountNr % 10);
            sum = sum + (dig * ((n + 1)));
            accountNr = accountNr / 10;
            n++;
        }

        
        int result = (int)(sum % bank.getDivResult());

        // רק בחישוב של בנק BNP PARIBAS
        // כאשר המשלים לשארית הוא 10 ספרת הביקורת חייבת להיות 0
        // כאשר המשלים לשארית הוא 11 ספרת הביקורת חייבת להיות 1
        if (bank.getBankCode() == 25 && checkDigit < 2)
        {
            checkDigit += 10;
        }
     
        //המשלים של השארית ל 11 - חייב להיות זהה לספרת הביקורת.
        if (bank.getDivResult() - result == checkDigit)
        {
            return Status.SUCCESS;
        }

        return Status.FAILED;

    }
}
