﻿
package shayja.bank.israel.strategy;

import shayja.bank.israel.Account;
import shayja.bank.israel.Bank;
import shayja.bank.israel.Branch;
import shayja.bank.israel.Status;

/**
 * Interface to be implemented by all check strategies
 */
public interface Strategy {

    /**
     * This method must calculate the check digit and return it within an check
     * digit object. It is called by the BankAccount class to calculate the
     * validity. If null is returned it means this strategy is not responsible
     * for the account code.
     *
     * @param bank
     *            The bank code
     * @param account
     *            The account code
     * @return The check digit of the account code
     *
     * @see CheckDigit
     */
    public Status calculateCheckDigit(final Bank bank, final Branch branchCode, final Account account);
}
