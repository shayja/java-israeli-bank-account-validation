﻿package shayja.bank.israel.strategy;

import shayja.bank.israel.Account;
import shayja.bank.israel.Bank;
import shayja.bank.israel.Branch;
import shayja.bank.israel.Status;


public class StrategyMasad implements Strategy {

    public Status calculateCheckDigit(final Bank bank,final Branch branch, final Account account) {

        // בדיקה 1 - זהה לקבוצת הפועלים
        if (new StrategyPoalim().calculateCheckDigit(bank, branch, account) == Status.SUCCESS)
        {
            return Status.SUCCESS;
        }

        // בדיקה 2 - זהה לקבוצת הבינלאומי
        Status result = new StrategyBenLeumi().calculateCheckDigit(bank, branch, account);
        if (result == Status.SUCCESS)
        {
            return Status.SUCCESS;
        }

        // כל שארית אחרת אינה חוקית.
        return Status.FAILED;

    }
}
