﻿package shayja.bank.israel.strategy;

import shayja.bank.israel.Account;
import shayja.bank.israel.Bank;
import shayja.bank.israel.Branch;
import shayja.bank.israel.Status;
import shayja.utils.ArrayUtils;



public class StrategyBenLeumi implements Strategy {

    public Status calculateCheckDigit(final Bank bank,final Branch branch, final Account account) {

	
    	/*=========================================נסיון ראשון=========================================*/

        // משתנים A B C D E F G H - X
    	long accountNr = account.getAccountCode();

        // משתנים D E F G H -X
        long shortAccountNumber = accountNr % 1000000;
        
        long n = 1;
        long sum = 0;
        int dig;
        long loop = accountNr > shortAccountNumber ? 10 : 7;

        // Counting from the rightmost digit, and moving left.
        while (n < loop)
        {
        	dig = (int)(accountNr % 10);
            sum = sum + (dig * (n));
            accountNr = accountNr / 10;
            n++;
        }
  
        int result = (int)(sum % bank.getDivResult());

        // בדיקת שארית
        if (ArrayUtils.contains(bank.getValidResult(), result))
        {
            return Status.SUCCESS;
        }

        /*=========================================נסיון שני=========================================*/

        //  חשבונות שנמצאו שגויים לפי חישוב זה יעברו לשלב ב' בו תתבצע בדיקה נוספת זהה לראשונה אולם רק על 6 הספרות הימניות של מספר החשבון 

        // Run only once - if 'loop' equals to 10 it means that the account number has more than 6 digits (greater than 'shortAccountNumber').
        if (loop == 10)
        {
        	Account account2nd = new Account(Long.toString(shortAccountNumber));
            return calculateCheckDigit(bank, branch, account2nd);
        }

        // חשבון שימצא שגוי גם בבדיקה של שלב ב' יחשב סופית כשגוי.
        return Status.FAILED;

    }
}
