﻿package shayja.bank.israel.strategy;

import shayja.bank.israel.Account;
import shayja.bank.israel.Bank;
import shayja.bank.israel.Branch;
import shayja.bank.israel.Status;
import shayja.utils.ArrayUtils;



public class StrategyDiscount implements Strategy {

    public Status calculateCheckDigit(final Bank bank, final Branch branch, final Account account) {

        // משתנים של קבוצת פועלים A B C D E F G H - X
        // משתנים של בנק הדואר I H G F E D C B A
        // למרות השוני בשמות המשתנים - ההכפלה זהה לחלוטין
    	long accountNr = account.getAccountCode();

        // יש לבצע בדיקה זו רק עבור בנק הדואר
        if ((bank.getBankCode() == 9) && (accountNr / 10000000 == 0))
        {
            return Status.ACCOUNT_CODE_TOO_SHORT;
        }

        accountNr = accountNr % 1000000000;
        int n = 1;
        long sum = 0;
        // Counting from the rightmost digit, and moving left.
        while (n < 10)
        {
        	int dig = (int)(accountNr % 10);
            sum = sum + (dig * (n));
            accountNr = accountNr / 10;
            n++;
        }

        int result = (int)(sum % bank.getDivResult());

        // בדיקת שארית
        if (ArrayUtils.contains(bank.getValidResult(), result))
        {
            return Status.SUCCESS;
        }

        // כל שארית אחרת אינה חוקית.
        return Status.FAILED;

    }
}
