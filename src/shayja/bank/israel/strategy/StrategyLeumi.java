﻿package shayja.bank.israel.strategy;

import shayja.bank.israel.Account;
import shayja.bank.israel.Bank;
import shayja.bank.israel.Branch;
import shayja.bank.israel.Status;
import shayja.utils.ArrayUtils;

public class StrategyLeumi implements Strategy {

    public Status calculateCheckDigit(final Bank bank,final Branch branch, final Account account) {

	
    	 long accountNr = account.getAccountCode();

         //  H X  להוסיף כמספר בן שתי ספרות את המשתנים.
         long sum = accountNr % 100;

         // משתנים B C D E F G
         accountNr = accountNr / 100;

         int dig;
         int n = 1;
         while (n < 7)
         {
             dig = (int)(accountNr % 10);
             sum = sum + (dig * (n + 1));
             accountNr = accountNr / 10;
             n++;
         }

         // משתנים S T U
         int branchNr = branch.getBranchCode();

         while (n < 10)
         {
             dig = branchNr % 10;
             sum = sum + (dig * (n + 1));
             branchNr = branchNr / 10;
             n++;
         }

         // מתעלמים מכל הספרות למעט היחידות והעשרות
         int result = (int)(sum % 100);
         

         // כולל רק עשרות ואחדות, נשאר רק לבדוק האם התוצאה שווה 20|60|70|72|90 result-לאחר ש
    	 if(ArrayUtils.contains(bank.getValidResult(), result)){
             return Status.SUCCESS;
         }

         // כל שארית אחרת אינה חוקית.
         return Status.FAILED;
    	

    }
    

}
