﻿package shayja.bank.israel.strategy;

import java.util.Map;

import shayja.bank.israel.Account;
import shayja.bank.israel.Bank;
import shayja.bank.israel.Branch;
import shayja.bank.israel.Status;
import shayja.utils.ArrayUtils;


public class StrategyPoalim implements Strategy {

	public Status calculateCheckDigit(final Bank bank,final Branch branch, final Account account) {

		// מס' חשבון בבנק הפועלים מורכב מ-5 ספרות + ספרת בקורת
		// משתנים D E F G H - X
		long accountNr = account.getAccountCode() % 1000000;


		// משתנים S T U
		int branchNr = branch.getBranchCode();

		// תנאי ייחודי לבנק המזרחי: לכל מספר סניף עם ערך גדול מ-400 יש להחסיר 400 לצורך החישוב
		if (bank.getBankCode() == 20/*המזרחי*/ && branchNr > 400)
		{
			branchNr = (branchNr - 400);
		}

		int n = 1;
		long sum = 0;
		int dig;

		// Counting from the rightmost digit, and moving left.
		while (n<7)
		{
			dig = (int)(accountNr % 10);
			sum = sum + (dig*(n));
			accountNr = accountNr/10;
			n++;
		}

		while (n < 10)
		{
			dig = branchNr % 10;
			sum = sum + (dig * (n));
			branchNr = branchNr / 10;
			n++;
		}

		// יש לחלק את התוצאה של ההכפלה בשארית שהוגדרה לבנק
		int result = (int)(sum % bank.getDivResult());

		// בדיקת שארית
		if (ArrayUtils.contains(bank.getValidResult(), result))
		{
			return Status.SUCCESS;
		}

		// חוקיות הבדיקה תלויה בשארית + מספר הסניף כך שבכל קבוצת סניפים חוקיות השארית משתנה 
		if (bank.getValidBranchResults() != null)
		{
			branchNr = branch.getBranchCode();

			// חוקיות הבדיקה תלויה בשארית + מספר הסניף כך שבכל קבוצת סניפים חוקיות השארית משתנה 
			for (Map.Entry<int[], int[]> entry : bank.getValidBranchResults().entrySet()) {
				if (ArrayUtils.contains(entry.getKey(), result)  && ArrayUtils.contains(entry.getValue(), branchNr))
				{
					return Status.SUCCESS;
				}


			}


		}

		// כל שארית אחרת אינה חוקית.
		return Status.FAILED;

	}
}
