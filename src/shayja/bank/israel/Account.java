﻿package shayja.bank.israel;

import shayja.utils.StringUtils;


public class Account {

    /** The minimal length for an account code */
    public static final int ACCOUNT_CODE_MIN_LENGTH = 3;
    /** The maximum length for an account code */
    public static final int ACCOUNT_CODE_MAX_LENGTH = 14;
    /** The padded account code */
    private final long accountCode;

    /**
     * The default-CTOR for the account code object
     *
     * @param accountCode
     *            The account code this object represents
     *
     * @throws IllegalArgumentException
     *             Thrown if the supplied account code is invalid
     */
    public Account(final String accountCode) throws IllegalArgumentException {

	if (null == accountCode) {
	    throw new IllegalArgumentException("Null pointer supplied as account code.");
	}

	/** 
	 * remove non numeric chars like whitespace, minus etc.
	 * The public method accepts any format of user input, eg. (xx-xxxxxx, xxx xxxxxx).
	 * The validation strategy method accepts digits only so we have to remove any non-numeric chars.
	 */ 
	String accountCodeClean = StringUtils.removeNonNumericChars(accountCode);
	
	if (accountCodeClean.length() < ACCOUNT_CODE_MIN_LENGTH) {
		throw new IllegalArgumentException(Integer.toString(Status.ACCOUNT_CODE_TOO_SHORT.getCode()));
	    //throw new IllegalArgumentException("Supplied account code is too short. It must have at least "+ ACCOUNT_CODE_MIN_LENGTH + " digits. [accountCode='" + accountCodeClean + "']");
	}

	if (accountCodeClean.length() > ACCOUNT_CODE_MAX_LENGTH) {
		throw new IllegalArgumentException(Integer.toString(Status.ACCOUNT_CODE_TOO_SHORT.getCode()));
	    //throw new IllegalArgumentException( "Supplied account code is too long. It may have at most " + ACCOUNT_CODE_MAX_LENGTH+ " digits. [accountCode='" + accountCodeClean + "']");
	}

	try {
		this.accountCode = Long.parseLong(accountCodeClean);
	} catch (Exception e) {
		e.printStackTrace();
        // account number contains invalid characters.
		throw new IllegalArgumentException(Integer.toString(Status.INVALID_ACCOUNT_CODE.getCode()));
		
	}


    }

    /**
     * Returns the account code as padded string
     *
     * @return The accountCode.
     */
    public long getAccountCode() {
	return accountCode;
    }  
    
    /**
     * @see java.lang.Object#toString()
     */
    public String toString() {
	return Long.toString(accountCode);
    }
}
