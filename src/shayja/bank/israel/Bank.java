﻿package shayja.bank.israel;

import java.util.Map;

import shayja.bank.israel.strategy.Strategy;


public class Bank {

    /** The minimal allowed length for a bank code */
    private final static int BANK_CODE_MIN_LENGTH = 1;
    /** The maximum allowed length for a bank code */
    private final static int BANK_CODE_MAX_LENGTH = 3;
    
    /** Stores the bank code */
    private final int bankCode;
    
	/**
	Stores the bank name.
	*/
    private String name;
	
	/**
	Stores the list of active branches. 
	*/
    private int[] branches;
	
	/**
	** Stores the list of closed branches. 
	*/
	private int[] closedbranches;
	
	
	/**
	** Stores the div result. 
	*/
	private int divResult;
	
	/**  
	** Stores the valid result.
	*/
	private int[] validResult;
	
	
	/**  
	** Stores the account validator strategy class. 
	*/
	private Strategy validator;
	
	/** 
	** Stores the valid branch results.
	*/
	private Map<int[], int[]> validBranchResults;
   
    /**
     * Default-CTOR for bank code objects
     *
     * @param bankCode
     *            The bank code this object represents
     */
    public Bank(final String bankCode) {

	if (null == bankCode) {
		throw new IllegalArgumentException(Integer.toString(Status.EMPTY_BANK_CODE.getCode()));
	    //throw new IllegalArgumentException("Null pointer supplied as bank code.");
	}

	if (bankCode.length() < BANK_CODE_MIN_LENGTH) {
		throw new IllegalArgumentException(Integer.toString(Status.BANK_CODE_TOO_SHORT.getCode()));
	    //throw new IllegalArgumentException("Supplied bank code is too short. It must have at least " + BANK_CODE_MIN_LENGTH + " digits. [bankCode='" + bankCode + "']");
	}

	if (bankCode.length() > BANK_CODE_MAX_LENGTH) {
		throw new IllegalArgumentException(Integer.toString(Status.BANK_CODE_TOO_LONG.getCode()));
	    //throw new IllegalArgumentException("Supplied bank code is too long. It may have at most " + BANK_CODE_MAX_LENGTH + " digits. [accountCode='"  + bankCode + "']");
	}

	for (int i = 0; i < bankCode.length(); ++i) {
	    char d = bankCode.charAt(i);
	    // Checks if the character is one of 0 to 9 (48 is ascii for 0, 57 is ascii for 9)
	    if (!(d >= 48 && d <= 57)) {
	    	throw new IllegalArgumentException(Integer.toString(Status.INVALID_BANK_CODE.getCode()));
	    	//throw new IllegalArgumentException("The supplied bank code must only consist of decimal numbers from 0 to 9. [bankCode='" + bankCode + "']");
	    }
	}
	// valid bank code argument => set private field value.
    this.bankCode = Integer.parseInt(bankCode);

    }

    
    public void setName(String value) {
        this.name = value;
    }
    
    public void setBranches(int[] value) {
        this.branches = value;
    }
    
    public void setClosedBranches(int[] value) {
        this.closedbranches = value;
    }
    
    public void setDivResult(int value) {
        this.divResult = value;
    }
    
    public void setValidResult(int[] value) {
        this.validResult = value;
    }
    
    public void setValidator(Strategy value) {
        this.validator = value;
    }

    public void setValidBranchResults(Map<int[], int[]> value) {
        this.validBranchResults = value;
    }
    


    /**
     * @return Returns the bankCode.
     */
    public int getBankCode() {
    	return this.bankCode;
    }

    public String getName() {
    	return this.name;
    }

    public int[] getBranches() {
    	return this.branches;
    }

    public int[] getClosedBranches() {
    	return this.closedbranches;
    }

    public int getDivResult() {
    	return this.divResult;
    }

    public int[] getValidResult() {
    	return this.validResult;
    }

    public Strategy getValidator() {
    	return this.validator;
    }

    public Map<int[], int[]> getValidBranchResults() {
    	return this.validBranchResults;
    }
    
    
    /**
     * @see java.lang.Object#toString()
     */
    @Override
	public String toString() {
    	return Integer.toString(this.bankCode);
    }
}
