﻿package shayja.bank.israel;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import shayja.utils.ConfigSettings;



	/**
	 * The Handler for SAX Events.
	 */
public class BranchHelperXmlImpl implements BranchHelper {


	private static final Map<Integer,ArrayList<Branch>> listOfBranches = new HashMap<Integer,ArrayList<Branch>>();
	private static final String BRANCHES_FILE_PATH_KEY = "branches_file_path";
	
		
		
		static {
			try {
				init();
			} catch (Exception e) {
				System.err.println(e.getMessage());
				e.printStackTrace();
			}
		}

		private synchronized static void init() throws Exception {
			try {
				String path = ConfigSettings.getString(BRANCHES_FILE_PATH_KEY);
				getBranches(path);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

		
		private static void getBranches(String fileName){

			
			//get the factory
			DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
			Document dom = null;
			try {

				//Using factory get an instance of document builder
				DocumentBuilder db = dbf.newDocumentBuilder();

				//parse using builder to get DOM representation of the XML file
				dom = db.parse(fileName);


			}catch(ParserConfigurationException pce) {
				pce.printStackTrace();
			}catch(SAXException se) {
				se.printStackTrace();
			}catch(IOException ioe) {
				ioe.printStackTrace();
			}
			
			//get the root element
			Element docEle = dom.getDocumentElement();

			//get a node list of Branches
			NodeList nl = docEle.getElementsByTagName("BRANCH");
			if(nl != null && nl.getLength() > 0) {
				for(int i = 0 ; i < nl.getLength();i++) {

					//get the Branch element
					Element el = (Element)nl.item(i);

					//get the Branch object
					Branch branch = getBranchItem(el);

					int bankCode = branch.getBankCode();
					
					//add it to list
					if (!listOfBranches.containsKey(bankCode)) {
						listOfBranches.put(bankCode, new ArrayList<Branch>());
					}
					listOfBranches.get(bankCode).add(branch);
				}
			}

		}
		

		
		/**
		 * I take a xml element and the tag name, look for the tag and get
		 * the text content
		 * i.e for <employee><name>John</name></employee> xml snippet if
		 * the Element points to employee node and tagName is 'name' I will return John
		 */
		private static String getTextValue(Element ele, String tagName) {
			String textVal = null;
			
			
			try {
				NodeList nl = ele.getElementsByTagName(tagName);
				if(nl != null && nl.getLength() > 0) {
					Element el = (Element)nl.item(0);
					Node firstChild = el.getFirstChild();
					if (firstChild!=null){
						textVal = firstChild.getNodeValue();
					}
					
				}
				
			} catch (Exception e) {
				System.err.println(e.getMessage());
				e.printStackTrace();
			}
			
			
			
			

			return textVal;
		}


		/**
		 * Calls getTextValue and returns a int value
		 */
		private static int getIntValue(Element ele, String tagName) {
			//in production application you would catch the exception
			return Integer.parseInt(getTextValue(ele,tagName));
		}
		
		
		/**
		 * I take an Branch element and read the values in, create
		 * an Branch object and return it
		 */
		private static Branch getBranchItem(Element empEl) {

			//for each <Branch> element get text or int values of
			String branchCode = getTextValue(empEl,"Branch_Code");
			int bankCode = getIntValue(empEl,"Bank_Code");

			//String type = empEl.getAttribute("type");

			//Create a new Branch with the value read from the xml nodes
			Branch branch = new Branch(branchCode);
			
			branch.setName(getTextValue(empEl,"Branch_Name"));
			branch.setBankName(getTextValue(empEl,"Bank_Name"));
			branch.setAddress(getTextValue(empEl,"Branch_Address"));
			branch.setZipCode(getTextValue(empEl,"Zip_Code"));
			branch.setCity(getTextValue(empEl,"City"));
			branch.setPhone(getTextValue(empEl,"Telephone"));
			branch.setFax(getTextValue(empEl,"Fax"));
			branch.setClosedDate(getTextValue(empEl,"Date_Closed"));
			
			branch.setBankCode(bankCode);
			
			
			
			return branch;
			
			
		}



		@Override
		public ArrayList<Branch> getBranchesByBank(int bankCode) {
			return listOfBranches.get(bankCode);
		}
	 
		@Override
		public Branch getBranch(int bankCode, int branchCode) {
			ArrayList<Branch> branchList = getBranchesByBank(bankCode);
			Branch branch = branchList.get(branchCode);
			return branch; 
		}
		
		
		
		
		
		
		
	
	    
	}
