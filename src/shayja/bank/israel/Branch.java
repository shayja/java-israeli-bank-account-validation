﻿package shayja.bank.israel;

import shayja.utils.StringUtils;

public class Branch {

	/** The minimal allowed length for a branch code */
	private final static int BRANCH_CODE_MIN_LENGTH = 1;
	/** The maximum allowed length for a branch code */
	private final static int BRANCH_CODE_MAX_LENGTH = 3;
	/** Stores the padded branch code */
	private final int branchCode;



	/** 
	 *  Gets or sets the Bank ID of the account holding branch.
	 *  קוד בנק
	 */
	private int bankCode;

	/** 
	 *  Gets or sets the name of the bank of the of the account holding branch.
	 */
	private String bankName;

	/** 
	 *  Gets or sets the name of the branch of the of the account holding branch.
	 */
	private String name;

	/** 
	 *  Gets or sets the branch address.
	 *  כתובת סניף
	 */
	private String address;

	/** 
	 *  Gets or sets the branch zip code.
	 *  מיקוד
	 */
	private String zipCode;

	/** 
	 *  Gets or sets the city that the branch is located.
	 *  יישוב
	 */
	private String city;

	/** 
	 *  Gets or sets the branch phone.
	 *  טלפון
	 */
	private String phone;

	/** 
	 *  Gets or sets the branch fax number.
	 *  פקס
	 */
	private String fax;

	/** 
	 *  Gets or sets the branch closed date.
	 *  תאריך סגירה
	 */
	private String closedDate;

	/**
	 * Default-CTOR for branch code objects
	 *
	 * @param branchCode
	 *            The branch code this object represents
	 */
	public Branch(final String branchCode) {

		if (null == branchCode) {
			throw new IllegalArgumentException("Null pointer supplied as branch code.");
		}

		if (branchCode.length() < BRANCH_CODE_MIN_LENGTH) {
			throw new IllegalArgumentException(Integer.toString(Status.BRANCH_CODE_TOO_SHORT.getCode()));
			//throw new IllegalArgumentException("Supplied branch code is too short. It must have at least "+ BRANCH_CODE_MIN_LENGTH + " digits. [branchCode='" + branchCode + "']");
		}

		if (branchCode.length() > BRANCH_CODE_MAX_LENGTH) {
			throw new IllegalArgumentException(Integer.toString(Status.BRANCH_CODE_TOO_LONG.getCode()));
			//throw new IllegalArgumentException("Supplied branch code is too long. It may have at most " + BRANCH_CODE_MAX_LENGTH + " digits. [accountCode='" + branchCode + "']");
		}

		for (int i = 0; i < branchCode.length(); ++i) {
			char d = branchCode.charAt(i);
			// Checks if the character is one of 0 to 9 (48 is ascii for 0, 57 is ascii for 9)
			if (!(d >= 48 && d <= 57)) {
				// if branch number is not numeric value => return INVALID_BRANCH_NUMBER status.
				throw new IllegalArgumentException(Integer.toString(Status.INVALID_BRANCH_CODE.getCode()));
				//throw new IllegalArgumentException("The supplied branch code must only consist of decimal numbers from 0 to 9. [branchCode='" + branchCode + "']");
			}
		}

		// remove non numeric chars like whitespace, minus etc.
		String branchCodeClean = StringUtils.removeNonNumericChars(branchCode);
		this.branchCode = Integer.parseInt(branchCodeClean);

	}



	public void setBankCode(int value) {
		this.bankCode = value;
	}

	public void setBankName(String value) {
		this.bankName = value;
	}

	public void setName(String value) {
		this.name = value;
	}

	public void setAddress(String value) {
		this.address = value;
	}

	public void setZipCode(String value) {
		this.zipCode = value;
	}

	public void setCity(String value) {
		this.city = value;
	}

	public void setPhone(String value) {
		this.phone = value;
	}

	public void setFax(String value) {
		this.fax = value;
	}

	public void setClosedDate(String value) {
		this.closedDate = value;
	}


	/**
	 * @return Returns the branchCode.
	 */
	 public int getBranchCode() {
		return branchCode;
	}

	public int getBankCode() {
		return this.bankCode;
	}

	public String getBankName() {
		return this.bankName;
	}

	public String getName() {
		return this.name;
	}

	public String getAddress() {
		return this.address;
	}

	public String getZipCode() {
		return this.zipCode;
	}

	public String getCity() {
		return this.city;
	}


	public String getPhone() {
		return this.phone;
	}

	public String getFax() {
		return this.fax;
	}

	public String getClosedDate() {
		return this.closedDate;
	}

	/**
	 * @see java.lang.Object#toString()
	 */
	 @Override
	public String toString() {
		return Integer.toString(this.branchCode);
	 }
}
