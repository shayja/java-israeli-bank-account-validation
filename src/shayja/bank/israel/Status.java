﻿package shayja.bank.israel;

import java.util.HashMap;
import java.util.Map;

public enum Status {
	SUCCESS(0, "הבדיקה הצליחה", "The test has passed."),
	FAILED(-1, "הבדיקה נכשלה", "The test was executed but failed."),

	/**
	 * Bank error codes
	 */
	BANK_CODE_NOT_SUPPORTED(-100, "בנק לא קיים ברשימה", "Bank code is not supported."),
	EMPTY_BANK_CODE(-101, "מספר בנק ריק", "No bank code was entered."),
	INVALID_BANK_CODE(-102, "מספר בנק לא חוקי", "The bank code contains illegel characters."),	
	BANK_CODE_TOO_LONG(-103, "מספר בנק ארוך מדי", "The bank code is too long."),
	BANK_CODE_TOO_SHORT(-104, "מספר בנק קצר מדי", "The bank code is too short."),
	/**
	 * Branch account codes
	 */
	EMPTY_BRANCH_CODE(-110, "מספר סניף ריק", "No branch code was entered."),
	INVALID_BRANCH_CODE(-111, "מספר סניף לא חוקי", "The branch code contains illegel characters."),	
	BRANCH_CODE_TOO_LONG(-112, "מספר סניף ארוך מדי", "The branch code is too long."),
	BRANCH_CODE_TOO_SHORT(-113, "מספר סניף קצר מדי", "The branch code is too short."),
	BRANCH_CODE_DOES_NOT_EXISTS(-114, "מספר סניף לא קיים ברשימה", "The branch code not found on branches list nor closed branches list."),				
	BRANCH_CLOSED(-115, "סניף הבנק נסגר", "The branch code found on the closed branches list"),
	/**
	 * Bank account codes
	 */
	EMPTY_ACCOUNT_CODE(-120, "מספר חשבון ריק", "No account code was entered."),
	INVALID_ACCOUNT_CODE(-121, "מספר חשבון לא תקין", "Account code contains illegel characters."),
	ACCOUNT_CODE_TOO_LONG(-122, "מספר חשבון ארוך מדי", "Account code is too long."),
	ACCOUNT_CODE_TOO_SHORT(-123, "מספר חשבון קצר מדי", "Account code is too short.");



	private int code;
	private String label;
	private String description;

	/**
	 * A mapping between the integer code and its corresponding Status to facilitate lookup by code.
	 */
	private static Map<Integer, Status> codeToStatusMapping;

	private Status(int code, String label, String description) {
		this.code = code;
		this.label = label;
		this.description = description;
	}

	public static Status getStatus(int i) {
		if (codeToStatusMapping == null) {
			initMapping();
		}
		Status result = codeToStatusMapping.get(i);
		return result;
	}

	private static void initMapping() {
		codeToStatusMapping = new HashMap<Integer, Status>();
		for (Status status : values()) {
			codeToStatusMapping.put(status.code, status);
		}
	}

	public int getCode() {
		return code;
	}

	public String getLabel() {
		return label;
	}

	public String getDescription() {
		return description;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder();
		sb.append("Status");
		sb.append("{code=").append(code);
		sb.append(", label='").append(label).append('\'');
		sb.append(", description='").append(description).append('\'');
		sb.append('}');
		return sb.toString();
	}
	/*
    public static void main(String[] args) {
        System.out.println(Status.PASSED);
        System.out.println(Status.getStatus(-1));
    }
	 */
}