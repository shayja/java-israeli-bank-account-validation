﻿package shayja.utils;

import java.util.Arrays;

public class ArrayUtils {
	
	public static boolean contains(final int[] array, final int key) {  
	     Arrays.sort(array); 
	     return Arrays.binarySearch(array, key) > -1;
	} 

}
