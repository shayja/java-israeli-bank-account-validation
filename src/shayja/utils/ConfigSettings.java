﻿package shayja.utils;

import java.io.*;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;



public class ConfigSettings {

	private static final String CONFIG_FILE_PATH = ".\\src\\config.properties";
	private static final Map<String,String> configSettingsList = new HashMap<String,String>();
	static {
		try {
			init(CONFIG_FILE_PATH);
		} catch (Exception e) {
			System.err.println(e.getMessage());
			e.printStackTrace();
		}
	}

	private static void init(final String filename) throws IOException {

		FileReader reader = new FileReader(filename);
		Properties props = new Properties();
		try {
			props.load(reader);
		} finally {
			reader.close();
		}
		for (Object key : props.keySet()) {
			configSettingsList.put(key.toString(), props.get(key).toString());
		}
	}
	
	public static String getString(final String key) {
		return configSettingsList.get(key);
	}
	
	public static int getInt(final String key) {
		return Integer.parseInt(configSettingsList.get(key));
	}
	
}