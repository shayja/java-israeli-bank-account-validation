﻿package shayja.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public final class StringUtils {

	 /* Get the value inside parentheses. 
     * Example: input String "One two three inside (123)" => will return '123'.
     */
	public static String extractValueFromParentheses( final String input)
    {
    	  String res = null;
          Pattern p = Pattern.compile("\\((.*?)\\)",Pattern.DOTALL);
          Matcher matcher = p.matcher(input);
          
          // iterate through matcher and return the last match.
          while(matcher.find())
          {
        	  res = matcher.group(1).toString();
          }
          return res;
    }
    
	public static boolean isInteger( String input )  
	{  
		if (null == input)
			return false;
			
		 boolean flag = true;
		 if (input.charAt(0) == '-') {
			 input = input.substring(input.length()-1, input.length());
		 }
		 for(int i=0; i<input.length() && flag; i++) {
		     flag=Character.isDigit(input.charAt(i));
		 }
		 return flag;
		
	} 
	
	public static String removeNonNumericChars( String input )
    {
		return input.replaceAll("[^\\d]", "");
    }
  


	
	
	
}
