import shayja.bank.israel.BankAccountValidator;
import shayja.bank.israel.BankAccountValidatorImpl;
import shayja.bank.israel.Status;

public class TestClass {

	public static void main(String[] args) {
		
		BankAccountValidator validation = new BankAccountValidatorImpl();
		
		Status status = validation.validateBankAccount("10", "607", "11710022");// Leumi bank test account number
		System.out.println(status.toString());  
		
		status = validation.validateBankAccount("11", "535", "000032018");// discount bank test account number
		System.out.println(status.toString());  
	
		status = validation.validateBankAccount("20", "406", "160778");// mizrahi bank test account number
		System.out.println(status.toString());  
		
		status = validation.validateBankAccount("31", "45", "000032018");// fibi bank test account number
		System.out.println(status.toString());  
		
	}

}
